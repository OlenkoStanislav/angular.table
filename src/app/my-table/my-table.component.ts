import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Product } from '../models/Product';
import { DataService } from '../services/DataService';

@Component({
  selector: 'app-my-table',
  templateUrl: './my-table.component.html',
  styleUrls: ['./my-table.component.scss']
})
export class MyTableComponent implements OnInit {

  public products: Product[] = [];

  @Input()
  public rows = 3;

  @Output()
  public eventEmitter: EventEmitter<number> = new EventEmitter<number>();
  public categories: string[] = [];
  public selectedCategory: string = "All";

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.getProducts();
    this.getCategories();
  }


  public remove(id: number) {
    this.dataService.deleteProductById(id);

    this.getProducts();

    this.eventEmitter.emit(id); this.getProducts();
  }

  public rowChange(event: any): void {
    this.rows = +event.target.value;

    this.getProducts();
  }
  public changeCategory(event: any): void {
    this.selectedCategory=event.target.value;
    this.getProducts();
  }
  private getCategories(): void {
    this.categories = this.dataService.getAllCategories();
  }

  private getProducts(): void {
    if (this.selectedCategory === "All")
      this.products = this.dataService.getAllProducts().slice(0, this.rows);
    else
      this.getProductsByCategory(this.selectedCategory);

  }
  private getProductsByCategory(category: string): void {
    this.products = this.dataService.getProductsByCategory(category).slice(0, this.rows);
  }

}
