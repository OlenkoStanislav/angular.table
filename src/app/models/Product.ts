export class Product {

    private static _idGenerator: number = 0;
    private _id: number = 0;
    private _name: string = "";
    private _price: number = 0;
    private _category: string = "";

    constructor(name: string, price: number, category: string) {
        this._id = ++Product._idGenerator;
        this._name = name;
        this._price = price;
        this._category = category;
    }

    get id(): number {
        return this._id;
    }

    get name(): string {
        return this._name;
    }
    set name(name: string) {
        if (name.length > 0)
            this._name = name;
        else
            throw Error("Wrong product name.")
    }


    get price(): number {
        return this._price;
    }
    set price(price: number) {
        if (price > 0)
            this._price = price;
        else
            throw Error("Price has to be more than zero")
    }


    get category(): string {
        return this._category;
    }
    set category(category: string) {
        this.category = category;
    }
}