import { Product } from "../models/Product";

export class DataService {

    private _products: Product[] = [];
    private _categories: string[] = ["category 1", "category 2", "category 3"];

    constructor() {
        this.initData();
    };

    private initData() {
        this._products.push(new Product("product1", 100, "category 1"));
        this._products.push(new Product("product2", 200, "category 2"));
        this._products.push(new Product("product3", 300, "category 3"));
        this._products.push(new Product("product4", 400, "category 1"));
        this._products.push(new Product("product5", 500, "category 2"));
        this._products.push(new Product("product6", 600, "category 3"));
        this._products.push(new Product("product7", 700, "category 1"));
        this._products.push(new Product("product8", 800, "category 2"));
        this._products.push(new Product("product9", 900, "category 3"));
        this._products.push(new Product("product10", 1000, "category 1"));
    }

    public getAllProducts(): Product[] {
        return [...this._products];
    }

    public deleteProductById(id: number): Product[] {

        this._products = this._products.filter(item => item.id != id);
        return [...this._products];
    }
    public getAllCategories(): string[] {
        return this._categories;
    }
    public getProductsByCategory(category: string): Product[] {
        return this._products.filter(f => f.category == category);
    }
}